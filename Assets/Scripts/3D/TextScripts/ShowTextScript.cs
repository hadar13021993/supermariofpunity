﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowTextScript : MonoBehaviour
{

    public GameObject text;
    public bool isEnterOnce = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !isEnterOnce)
        {
            isEnterOnce = true;
            text.SetActive(true);
        }
    }
}
