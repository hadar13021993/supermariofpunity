﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextScript : MonoBehaviour
{
    public GameObject text;

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            Destroy(text);
        }
    }
}
