﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LedgeMovementScript : MonoBehaviour
{
    public float topPoint = 155;
    public float bottomPoint = 146;
    public int direction = 1;

    private void Update()
    {
        if (direction == 1)
        {
            transform.Translate(Vector3.up * Time.deltaTime, Space.World);
            direction = 1;
        }

        if (this.transform.position.y > topPoint)
        {
            direction = 2;
        }

        if (direction == 2)
        {
            transform.Translate(Vector3.up * (-1) * Time.deltaTime, Space.World);
            direction = 2;
        }

        if (this.transform.position.y < bottomPoint)
        {
            direction = 1;
        }
    }
}
