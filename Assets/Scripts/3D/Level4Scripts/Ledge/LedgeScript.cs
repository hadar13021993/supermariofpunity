﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LedgeScript : MonoBehaviour
{
    public bool needToFall = false;
    public float fallSpeed = 40.0f;

    private void Update()
    {
        if (needToFall)
            transform.Translate(Vector3.down * fallSpeed * Time.deltaTime, Space.World);
    }
    private IEnumerator OnTriggerEnter(Collider other)
    {
        yield return new WaitForSeconds(2f);
        needToFall = true;
    }
}
