﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class level1_4 : MonoBehaviour
{
    public GameObject fadeScreen;
    public GameObject player;
    public GameObject winnerText;

    public AudioSource levelCompleteEffect;
    public AudioSource levelEffect;

    private int timeScore;
    private int timeLeft;
    private bool intoCastle = false;

    private IEnumerator OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !intoCastle)
        {
            winnerText.SetActive(true);
            intoCastle = true;
            levelEffect.Stop();
            levelCompleteEffect.Play();
            fadeScreen.SetActive(true);
            yield return new WaitForSeconds(8f);
            fadeScreen.GetComponent<Animator>().enabled = true;
            timeLeft = CountingDown.TimeLeft;
            timeScore = timeLeft * 10;
            GlobalScore.currenScore += timeScore;
            yield return new WaitForSeconds(0.495f);
            fadeScreen.GetComponent<Animator>().enabled = false;

            SceneManager.LoadScene(10);
        }
    }
}
