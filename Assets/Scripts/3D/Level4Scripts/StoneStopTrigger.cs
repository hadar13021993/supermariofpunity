﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneStopTrigger : MonoBehaviour
{
    public static bool isStoneStopTriggered = false;
    private bool enterOnce = false;

    public GameObject stone;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Stone" && !enterOnce && StoneTrigger.isStoneTriggered)
        {
            enterOnce = true;
            isStoneStopTriggered = true;
            Destroy(stone);
        }
    }
}
