﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneTrigger : MonoBehaviour
{
    
    public static bool isStoneTriggered = false;

    public GameObject PlayerCam;
    public GameObject SecCam;
    public GameObject text;

    public AudioSource shakeNoise;

    private bool enterOnce = false;

    private IEnumerator OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !enterOnce)
        {
            enterOnce = true;
            text.SetActive(true);
            SecCam.SetActive(true);
            PlayerCam.SetActive(false);
            shakeNoise.Play();
            yield return new WaitForSeconds(1.5f);
            isStoneTriggered = true;
            yield return new WaitForSeconds(1f);
            SecCam.SetActive(false);
            PlayerCam.SetActive(true);

        }
    }
}
