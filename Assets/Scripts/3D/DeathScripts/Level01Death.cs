﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level01Death : MonoBehaviour
{
    public AudioSource DeathSound;
    public GameObject LevelMusic;
    public GameObject thePlayer;

    private IEnumerator OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
                GlobalLives.livesAmount -= 1;
                LevelMusic.SetActive(false);
                thePlayer.SetActive(false);
                DeathSound.Play();
                yield return new WaitForSeconds(3);
                GlobalScore.currenScore = 0;
                GlobalCoins.coinsCount = 0;
                SceneManager.LoadScene(LevelCounted.scenePreloadCount);

        }
    }
}
