﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinScript : MonoBehaviour
{
    public AudioSource coinsEffect;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            coinsEffect.Play();
            GlobalCoins.coinsCount += 1;
            GlobalScore.currenScore += 10;
            Destroy(gameObject);
        }
    }
}
