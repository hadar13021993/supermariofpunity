﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideText2DScript : MonoBehaviour
{
    public GameObject text;
    public bool isEnter = false;

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player" && !isEnter)
        {
            isEnter = true;
            Destroy(text);
        }
    }
}
