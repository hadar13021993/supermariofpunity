﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagScript : MonoBehaviour
{
    public Transform flag;
    public Transform flagStop;
    private bool moveFlag = false;

    private float flagVelocityY = -.08f;

    void FixedUpdate()
    {
        if (moveFlag && flag.position.y > flagStop.position.y)
        {
            flag.position = new Vector2(flag.position.x, flag.position.y + flagVelocityY);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && !moveFlag)
        {
            moveFlag = true;
        }
    }
}
