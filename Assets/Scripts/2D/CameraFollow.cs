﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public bool keepFollow = true;
    public Transform target;
    public Transform leftBounds;
    public Transform rightBounds;
    public float smoothDampTime = 0.15f;
    private Vector3 smoothDampVlocity = Vector3.zero;
    private float camWidth, camHieght, levelMinX, levelMaxX;

    // Start is called before the first frame update
    void Start()
    {
        camHieght = Camera.main.orthographicSize * 2;
        camWidth = camHieght * Camera.main.aspect;

        float leftBuoundsWidth = leftBounds.GetComponentInChildren<SpriteRenderer>().bounds.size.x / 2;
        float rightBuoundsWidth = rightBounds.GetComponentInChildren<SpriteRenderer>().bounds.size.x /2 ;

        levelMinX = leftBounds.position.x + leftBuoundsWidth + (camWidth / 2);
        levelMaxX = rightBounds.position.x - rightBuoundsWidth - (camWidth / 2);
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        if (target && keepFollow)
        {
            float targetX = Mathf.Max(levelMinX, Mathf.Min(levelMaxX, target.position.x));
            float x = Mathf.SmoothDamp(transform.position.x, targetX, ref smoothDampVlocity.x, smoothDampTime);

            transform.position = new Vector3(x, transform.position.y, transform.position.z);
        }
    }

    public void SetKeepFollow(bool val) 
    {
        keepFollow = val; 
    }

}
