﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyScript : MonoBehaviour
{
    public float gravity;
    
    public Vector2 velocity;
    public bool isWalkingLeft = true;
    public LayerMask floorMask;
    public LayerMask wallMask;
    public AudioSource DeathSound;
    public GameObject LevelMusic;
    public GameObject Player;

    private bool grounded = false;
    private bool shouldDie = false;
    private float deathTimer = 0;
    private float timeBeforeDestroy = 0.01f;
    public AudioSource dieEffect;

    private enum EnemyState
    {
        Walking, Falling, Dead
    }

    private EnemyState enemyState = EnemyState.Falling;

    // Start is called before the first frame update
    void Start()
    {
        enabled = false;
        Fall();
    }

    // Update is called once per frame
    void Update()
    {
        ChackIfPlayerStillAlive();
        CheckCrashed();
    }

    public void Crash()
    {
        if (PlayerScript.stillAlive)
        {
            dieEffect.Play();
            enemyState = EnemyState.Dead;
            GetComponent<Collider2D>().enabled = false;
            shouldDie = true;
        }
    }

    public void Dead()
    {
        if (PlayerScript.stillAlive)
        {
            enemyState = EnemyState.Dead;
            GetComponent<Collider2D>().enabled = false;
            shouldDie = true;
        }
    }


    void CheckCrashed()
    {
        if (shouldDie)
        {
            if (deathTimer <= timeBeforeDestroy)
            {
                deathTimer += Time.deltaTime;
            }
            else
            {
                GlobalScore.currenScore += 100;
                shouldDie = false;
                Destroy(this.gameObject);
            }
        }
    }

    void UpdateEnemyPos()
    {
        if (enemyState != EnemyState.Dead)
        {
            Vector3 pos = transform.localPosition;
            Vector3 scale = transform.localScale;

            if (enemyState == EnemyState.Falling)
            {
                pos.y += velocity.y * Time.deltaTime;
                velocity.y -= gravity * Time.deltaTime;
            }

            if (enemyState == EnemyState.Walking)
            {
                if (isWalkingLeft)
                {
                    pos.x -= velocity.x * Time.deltaTime;
                    scale.x = -1;
                } 
                else
                {
                    pos.x += velocity.x * Time.deltaTime;
                    scale.x = 1;
                }

            }

            if (velocity.y <= 0) pos = CheckGround(pos);
           
            CheckWallRays(pos, scale.x);

            transform.localPosition = pos;
            transform.localScale = scale;
        }
    }

    Vector3 CheckGround(Vector3 pos) 
    {
        Vector2 originLeft = new Vector2(pos.x - 0.5f + 0.2f, pos.y - .5f);
        Vector2 originMiddle = new Vector2(pos.x , pos.y - .5f); 
        Vector2 originRight = new Vector2(pos.x + 0.5f - 0.2f, pos.y - .5f);

        RaycastHit2D groundLeft = Physics2D.Raycast(originLeft, Vector2.down, velocity.y * Time.deltaTime, floorMask);
        RaycastHit2D groundMiddle = Physics2D.Raycast(originMiddle, Vector2.down, velocity.y * Time.deltaTime, floorMask);
        RaycastHit2D grounfRight = Physics2D.Raycast(originRight, Vector2.down, velocity.y * Time.deltaTime, floorMask);

        if (groundLeft.collider != null || groundMiddle.collider != null || grounfRight.collider != null)
        {
            RaycastHit2D hitRay = groundLeft;

            if (groundLeft) hitRay = groundLeft;
            else if (groundMiddle) hitRay = groundMiddle;
            else if (grounfRight) hitRay = grounfRight;

            if (hitRay.collider.tag == "Player")
            {
                StartCoroutine(Death());
            }

            pos.y = hitRay.collider.bounds.center.y + hitRay.collider.bounds.size.y / 2 + 0.5f;
            grounded = true;
            velocity.y = 0;
            enemyState = EnemyState.Walking;
        }
        else
        {
            if (enemyState != EnemyState.Falling)
            {
                Fall();
            }
        }

        return pos;
    }

    void CheckWallRays(Vector3 pos, float dir) 
    {
        Vector2 originTop = new Vector2(pos.x + dir*0.4f, pos.y + .5f - 0.2f);
        Vector2 originMiddle = new Vector2(pos.x + dir * 0.4f, pos.y);
        Vector2 originBottom = new Vector2(pos.x + dir * 0.4f, pos.y - .5f + 0.2f);

        RaycastHit2D wallTop = Physics2D.Raycast(originTop, new Vector2(dir, 0), velocity.x * Time.deltaTime, wallMask);
        RaycastHit2D wallMiddle = Physics2D.Raycast(originMiddle, new Vector2(dir, 0), velocity.x * Time.deltaTime, wallMask);
        RaycastHit2D wallBottom = Physics2D.Raycast(originBottom, new Vector2(dir, 0), velocity.x * Time.deltaTime, wallMask);

        if (wallTop.collider != null || wallMiddle.collider != null || wallBottom.collider != null)
        {
            RaycastHit2D hitRay = wallTop;

            if (wallTop) hitRay = wallTop;
            else if (wallMiddle) hitRay = wallMiddle;
            else if (wallBottom) hitRay = wallBottom;

            if (hitRay.collider.tag == "Player")
            {
                StartCoroutine(Death());
            }

            isWalkingLeft = !isWalkingLeft;
        }
    }

    private IEnumerator Death()
    {
        PlayerScript.stillAlive = false;

        Player.GetComponent<Animator>().SetBool("isJumping", false);
        Player.GetComponent<Animator>().SetBool("isRunning", false);

        Player.GetComponent<Collider2D>().enabled = false;

        GlobalLives.livesAmount -= 1;
        LevelMusic.SetActive(false);
        DeathSound.Play();
        yield return new WaitForSeconds(3);
        GlobalScore.currenScore = 0;
        GlobalCoins.coinsCount = 0;
        SceneManager.LoadScene(LevelCounted.scenePreloadCount);

    }

    void OnBecameVisible()
    {
        enabled = true;
    }

    void ChackIfPlayerStillAlive()
    {
        if (PlayerScript.stillAlive)
            UpdateEnemyPos();
    }

    void Fall()
    {
        velocity.y = 0;
        enemyState = EnemyState.Falling;
        grounded = false;
    }
}
