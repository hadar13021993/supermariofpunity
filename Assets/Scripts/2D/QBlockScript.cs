﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QBlockScript : MonoBehaviour
{
    public float bounceHeight = 0.5f;
    public float bounceSpeed = 4f;
    public Vector2 originPos;
    public Sprite emptyBlockSprit;
    public float coinMoveSpeed = 8f;
    public float coinMoveHeight = 3f;
    public float coinFallDis = 2f;
    public AudioSource coinsEffect;

    private bool canBounce = true;

    void Start()
    {
        originPos = transform.localPosition;
    }

    public void QBlockBounced()
    {
        if (canBounce)
        {
            canBounce = false;
            StartCoroutine(Bounce());
        }
    }

    void ChangeSprite()
    {
        GetComponent<Animator>().enabled = false;
        GetComponent<SpriteRenderer>().sprite = emptyBlockSprit;
    }

    void PresentCoin()
    {
        GameObject spinningCoin = (GameObject)Instantiate(Resources.Load("Prefabs/Spinning_Coin", typeof(GameObject)));
        spinningCoin.transform.SetParent(this.transform.parent);
        spinningCoin.transform.localPosition = new Vector2(originPos.x, originPos.y + 1);

        coinsEffect.Play();
        GlobalCoins.coinsCount += 1;
        GlobalScore.currenScore += 10;

        StartCoroutine(MoveCoin(spinningCoin));
    }

    private IEnumerator Bounce()
    {
        ChangeSprite();
        PresentCoin();

        while (true)
        {
            transform.localPosition = new Vector2(transform.localPosition.x, transform.localPosition.y + bounceSpeed * Time.deltaTime);
            if (transform.localPosition.y >= originPos.y + bounceHeight)
            {
                break;
            }

            yield return null;
        }

        while (true)
        {
            transform.localPosition = new Vector2(transform.localPosition.x, transform.localPosition.y - bounceSpeed * Time.deltaTime);
            if (transform.localPosition.y <= originPos.y)
            {
                transform.localPosition = originPos;
                break;
            }

            yield return null;
        }
    }

    IEnumerator MoveCoin(GameObject coin)
    {
        while (true)
        {
            coin.transform.localPosition = new Vector2(coin.transform.localPosition.x, coin.transform.localPosition.y + coinMoveSpeed * Time.deltaTime);
            if (coin.transform.localPosition.y >= originPos.y + coinMoveHeight)
            {
                break;
            }

            yield return null;
        }

        while (true)
        {
            coin.transform.localPosition = new Vector2(coin.transform.localPosition.x, coin.transform.localPosition.y - coinMoveSpeed * Time.deltaTime);
            if (coin.transform.localPosition.y <= originPos.y + coinFallDis + 1)
            {
                Destroy(coin.gameObject);
                break;
            }

            yield return null;
        }
    }

}
