﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockScript : MonoBehaviour
{
    public float bounceHeight = 0.5f;
    public float bounceSpeed = 4f;
    public Vector2 originPos;
    public AudioSource brickSound;

    private bool canBounce = true;

    void Start()
    {
        originPos = transform.localPosition;
    }

    public void QBlockBounced()
    {
        StartCoroutine(Bounce());
    }

    private IEnumerator Bounce()
    {
        brickSound.Play();

        while (true)
        {
            transform.localPosition = new Vector2(transform.localPosition.x, transform.localPosition.y + bounceSpeed * Time.deltaTime);
            if (transform.localPosition.y >= originPos.y + bounceHeight)
            {
                break;
            }

            yield return null;
        }

        while (true)
        {
            transform.localPosition = new Vector2(transform.localPosition.x, transform.localPosition.y - bounceSpeed * Time.deltaTime);
            if (transform.localPosition.y <= originPos.y)
            {
                transform.localPosition = originPos;
                break;
            }

            yield return null;
        }
    }
}
