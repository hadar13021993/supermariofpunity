﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pipe0031Entry : MonoBehaviour
{
    public int StoodOn;
    public Vector2 velocity;

    public GameObject mainPlayer;
    public GameObject mainCam;
    public GameObject secCam;

    public GameObject fadeScreen;
    public AudioSource pipeEffect;
    public AudioSource pipeEffectOut;

    private IEnumerator OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            mainCam.GetComponent<CameraFollow>().SetKeepFollow(false);

            pipeEffect.Play();

            fadeScreen.SetActive(true);
            mainPlayer.SetActive(false);
            yield return new WaitForSeconds(1.5f);
            fadeScreen.GetComponent<Animator>().enabled = true;
            yield return new WaitForSeconds(0.495f);
            fadeScreen.GetComponent<Animator>().enabled = false;
            secCam.SetActive(true);
            mainCam.SetActive(false);
            yield return new WaitForSeconds(0.495f);
            fadeScreen.GetComponent<Animator>().enabled = false;
            fadeScreen.SetActive(false);
            mainPlayer.SetActive(true);
            mainPlayer.transform.position = new Vector3(71.3f, 10, 0);

            secCam.GetComponent<CameraFollow>().SetKeepFollow(true);

            pipeEffectOut.Play();
        }
    }
}
