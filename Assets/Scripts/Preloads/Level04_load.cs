﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level04_load : MonoBehaviour
{
    IEnumerator Start()
    {
        LevelCounted.levelCount = 4;
        LevelCounted.scenePreloadCount = 7;

        yield return new WaitForSeconds(5);
        PlayerScript.stillAlive = true;
        SceneManager.LoadScene(8);
    }
}
