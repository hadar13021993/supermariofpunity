﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level02_load : MonoBehaviour
{
    IEnumerator Start()
    {
        LevelCounted.levelCount = 2;
        LevelCounted.scenePreloadCount = 3;

        yield return new WaitForSeconds(5);
        PlayerScript.stillAlive = true;
        SceneManager.LoadScene(4);
    }
}
