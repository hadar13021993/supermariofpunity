﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level03_load : MonoBehaviour
{
    IEnumerator Start()
    {
        LevelCounted.levelCount = 3;
        LevelCounted.scenePreloadCount = 5;

        yield return new WaitForSeconds(5);
        PlayerScript.stillAlive = true;
        SceneManager.LoadScene(6);
    }
}
